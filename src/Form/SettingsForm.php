<?php

namespace Drupal\migrate_visualize\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Graphp\GraphViz\GraphViz;
use Fhaculty\Graph\Graph;
use Fhaculty\Graph\Exception\UnexpectedValueException;

/**
 * Configure Migrate Visualize settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'migrate_visualize_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['migrate_visualize.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['display_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Select visualization method:'),
      '#options' => [
        'mermaidjs' => $this->t('MermaidJS'),
        'graphviz' => $this->t('GraphViz'),
      ],
      'mermaidjs' => [
        '#description' => $this->t('Charts rendered in browser (uses CDN for MermaidJS library).'),
      ],
      'graphviz' => [
        '#description' => $this->t('Charts rendered on server (requires <strong>graphviz</strong> installed on server).'),
      ],
      '#default_value' => $this->config('migrate_visualize.settings')->get('display_mode'),
    ];

    try {
      // Test if GraphViz works in this environment.
      $graphViz = new GraphViz();
      $graphViz->createImageHTML(new Graph());
    }
    catch (UnexpectedValueException $exception) {
      // GraphViz doesn't work in this environment.
      $form['display_mode']['graphviz']['#disabled'] = TRUE;
      $form['display_mode']['graphviz']['#description'] .= ' ' . $this->t('Unavailable - see <a href="@docs">documentation</a>.', [
        '@docs' => 'https://git.drupalcode.org/project/migrate_visualize/-/tree/1.0.x#graphviz-server-install',
      ]);
      $this->messenger()->addWarning('GraphViz can not be selected as the server requirements are not met.');
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('migrate_visualize.settings')
      ->set('display_mode', $form_state->getValue('display_mode'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

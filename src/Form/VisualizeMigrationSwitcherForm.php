<?php

namespace Drupal\migrate_visualize\Form;

use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBase;

/**
 * Provides a form that activates a different workspace.
 */
class VisualizeMigrationSwitcherForm extends FormBase {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * The migration plugin manager.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected $migrationPluginManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The form constructor.
   *
   * @param \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migration_plugin_manager
   *   The migration plugin manager.
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   The module handler.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The route matcher.
   */
  public function __construct(MigrationPluginManagerInterface $migration_plugin_manager, ModuleHandler $module_handler, CurrentRouteMatch $current_route_match) {
    $this->migrationPluginManager = $migration_plugin_manager;
    $this->moduleHandler = $module_handler;
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.migration'),
      $container->get('module_handler'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'migrate_visualize_migration_switcher';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = [];

    $migrations = $this->migrationPluginManager->createInstances('');
    foreach ($migrations as $migration) {
      $options[$migration->id()] = $this->t('@label [@id]', [
        '@id' => $migration->id(),
        '@label' => $migration->label() ? $migration->label() : 'Unlabelled form',
      ]);
    }

    $form['migration'] = [
      '#type' => 'select',
      '#title' => $this->t('Switch to migration'),
      '#options' => $options,
      '#wrapper_attributes' => [
        'class' => ['container-inline'],
      ],
      '#attributes' => [
        'class' => ['migration-switcher'],
      ],
      '#attached' => [
        'library' => [
          'migrate_visualize/migrate_visualize',
        ],
      ],
    ];

    if ($current = $this->currentRouteMatch->getParameter('migration')) {
      $form['migration']['#default_value'] = $current;
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Switch'),
      '#button_type' => 'primary',
    ];
    $form['actions']['#wrapper_attributes'] = [
      'class' => ['container-inline'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('migrate_visualize.visualize', [
      'migration' => $form_state->getValue('migration'),
    ]);
  }

}

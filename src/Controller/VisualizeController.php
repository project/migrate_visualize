<?php

namespace Drupal\migrate_visualize\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate_visualize\MigrateGraph;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for visualization route.
 */
class VisualizeController extends ControllerBase {

  /**
   * The migration plugin manager.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected $migrationPluginManager;

  /**
   * The migration graph service.
   *
   * @var \Drupal\migrate_visualize\MigrateGraph
   */
  protected $migrationGraph;

  /**
   * The controller constructor.
   *
   * @param \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migration_plugin_manager
   *   The migration plugin manager.
   * @param Drupal\migrate_visualize\MigrateGraph $migrate_graph
   *   The migration graph service.
   */
  public function __construct(MigrationPluginManagerInterface $migration_plugin_manager, MigrateGraph $migrate_graph) {
    $this->migrationPluginManager = $migration_plugin_manager;
    $this->migrationGraph = $migrate_graph;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.migration'),
      $container->get('migrate_visualize.migrate_graph')
    );
  }

  /**
   * Visualize the migration.
   *
   * @todo Can the parameter be upcast?
   * @see https://www.drupal.org/docs/8/api/routing-system/parameters-in-routes/how-upcasting-parameters-works
   */
  public function build(string $migration) {
    $build['switcher'] = $this->formBuilder()
      ->getForm('Drupal\migrate_visualize\Form\VisualizeMigrationSwitcherForm');

    $migrationInstance = $this->migrationPluginManager->createInstance($migration);
    if ($migration !== '' && $migrationInstance === FALSE) {
      $this->messenger()
        ->addError($this->t('Migration not found: @migration', [
          '@migration' => $migration,
        ]));
      throw new NotFoundHttpException();
    }

    try {
      $this->migrationGraph->graph($migrationInstance);
    }
    catch (\Exception $exception) {
      $this->messenger()
        ->addError($this->t('@class Unable to graph migration: @message', [
          '@message' => $exception->getMessage(),
          '@class' => get_class($exception),
        ]));
      // There's a partial graph built, but we won't display it.
      return $build;
    }

    $build['visualize'] = [
      '#migration' => $migrationInstance,
      '#weight' => 110,
    ];

    /** @var \Fhaculty\Graph\Graph $build['visualize']['#graph'] */
    $build['visualize']['#graph'] = $this->migrationGraph->getGraph();

    switch ($this->config('migrate_visualize.settings')->get('display_mode')) {
      case 'mermaidjs':
        $build['visualize']['#attached']['library'][] = 'migrate_visualize/mermaid';
        $build['visualize']['#theme'] = 'migration_visualize_mermaid';
        break;

      case 'graphviz':
        $build['visualize']['#theme'] = 'migration_visualize_graphviz';
        break;
    }

    return $build;
  }

}

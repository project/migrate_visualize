<?php

namespace Drupal\migrate_visualize\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Extension\ModuleHandler;

/**
 * Lists available migrations.
 */
class ListingController extends ControllerBase {

  /**
   * The migration plugin manager.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected $migrationPluginManager;

  /**
   * Drupal\Core\Extension\ModuleHandler definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The controller constructor.
   *
   * @param \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migration_plugin_manager
   *   The migration plugin manager.
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   The module handler service.
   */
  public function __construct(MigrationPluginManagerInterface $migration_plugin_manager, ModuleHandler $module_handler) {
    $this->migrationPluginManager = $migration_plugin_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.migration'),
      $container->get('module_handler')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {
    $migrations = $this->migrationPluginManager->createInstances('');
    $items = [];

    foreach ($migrations as $migration) {
      $items[] = Link::fromTextAndUrl($migration->label(), Url::fromRoute('migrate_visualize.visualize', [
        'migration' => $migration->id(),
      ]));
    }

    if (!empty($items)) {
      $build['intro'] = [
        '#type' => 'markup',
        '#markup' => 'Click a migration below to visualize it:',
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      ];

      $build['list'] = [
        '#theme' => 'item_list',
        '#items' => $items,
      ];
    }
    else {
      $build['no_results'] = [
        '#type' => 'markup',
        '#markup' => 'No migrations could be loaded.',
      ];
    }

    return $build;
  }

}

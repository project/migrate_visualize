(function (Drupal, drupalSettings, $) {
  Drupal.behaviors.migrateVisualize = {
    // Copy functionality for diagram source.
    copySource: () => {
      document.querySelector('.migration-source textarea').select();
      document.execCommand('copy');
    },

    switchToVisualizeMigration: (event) => {
      $('.migrate-visualize-migration-switcher').submit();
    },

    attach(context) {
      if (context === document) {
        $('.migration-source .copy-source').on('click', this.copySource);
        $('.migrate-visualize-migration-switcher .migration-switcher').on(
          'change',
          this.switchToVisualizeMigration,
        );
        $('.migrate-visualize-migration-switcher .form-actions').hide();
      }
    },
  };
})(Drupal, drupalSettings, jQuery);

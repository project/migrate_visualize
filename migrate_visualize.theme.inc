<?php

/**
 * @file
 * Theme functions for Migrate Visualize.
 */

use Graphp\GraphViz\GraphViz;
use Drupal\Component\Utility\Html;
use Fhaculty\Graph\Exception\UnexpectedValueException;

/**
 * Provide an array of graph object types and default attributes.
 *
 * @return array
 *   The mapping of objects to attributes.
 */
function _migrate_visualize_graph_type_attributes() {
  return [
    'source' => ['color' => 'red', 'shape' => 'cds'],
    'source:meta' => ['color' => 'white', 'shape' => 'box'],
    'source:field' => ['color' => 'red', 'shape' => 'cds'],
    'destination:field' => ['color' => 'darkgoldenrod3', 'shape' => 'box'],
    'destination:meta' => ['color' => 'white', 'shape' => 'cds'],
    'process' => ['color' => 'blue', 'shape' => 'box'],
    'process:pipeline' => ['color' => 1, 'shape' => 'box'],
    'process:meta' => ['color' => 2, 'shape' => 'box'],
    'process:pipeline:step' => ['color' => 3, 'shape' => 'box'],
    'plugin' => ['color' => 'cornflowerblue2', 'shape' => 'box'],
    'destination' => ['color' => 'darkgoldenrod3', 'shape' => 'box'],
  ];
}

/**
 * Prepares variables for migration-visualize template.
 *
 * Default template: migration-visualize-graphviz.html.twig.
 *
 * @param array $variables
 *   Variables array.
 */
function template_preprocess_migration_visualize_graphviz(array &$variables) {
  /** @var \Fhaculty\Graph\Graph $graph */
  $graph = $variables['graph'];

  // Base edge colors on origin vertex type.
  $edgeTypes = [
    'source' => ['colorscheme' => 'bupu9'],
    'source:field' => ['colorscheme' => 'bupu9'],
    'process' => ['colorscheme' => 'bupu9'],
    'process:field' => ['colorscheme' => 'bupu9'],
    'process:pipeline:step' => ['colorscheme' => 'bupu9'],
    'plugin' => ['colorscheme' => 'bupu9'],
    'process:plugin' => ['colorscheme' => 'bupu9'],
    'destination' => ['color' => 'goldenrod3'],
    'destination:field' => ['color' => 'goldenrod3'],
  ];

  $graph->setAttribute('graphviz.graph.charset', 'iso-8859-1');
  $graph->setAttribute('graphviz.graph.rankdir', 'LR');

  $typeAttributes = _migrate_visualize_graph_type_attributes();

  /** @var \Fhaculty\Graph\Vertex $vertex */
  foreach ($graph->getVertices() as $k => $vertex) {
    if ($type = $vertex->getAttribute('migrate_visualize.type')) {
      $groups = ['source' => 0, 'process' => 1, 'destination' => 2];
      $groupId = explode(':', $type)[0];
      if (isset($groups[$groupId])) {
        $vertex->setGroup($groups[$groupId]);
      }
    }

    $vertex->setAttribute('graphviz.class', HTML::cleanCssIdentifier(str_replace(':', '-', $vertex->getId())));

    $vertexType = $vertex->getAttribute('migrate_visualize.type');
    if ($label = $vertex->getAttribute('migrate_visualize.label')) {
      $vertex->setAttribute('graphviz.label', $label);
    }
    if (isset($typeAttributes[$vertexType])) {
      foreach ($typeAttributes[$vertexType] as $attributeKey => $attributeValue) {
        $vertex->setAttribute("graphviz.{$attributeKey}", $attributeValue);
      }
    }
  }

  $graphvizAttributes = [
    'colorscheme' => 'dark28',
    'penwidth' => 2,
  ];

  /** @var \Fhaculty\Graph\Edge\Directed $edge */
  foreach ($graph->getEdges() as $k => $edge) {
    $edge->setAttribute('graphviz.color', $k % 6 + 2);
    foreach ($graphvizAttributes as $name => $value) {
      if (!$edge->getAttribute($name)) {
        $edge->setAttribute("graphviz.{$name}", $value);
      }
    }
    if ($comment = $edge->getAttribute('migrate_visualize.debug')) {
      $edge->setAttribute('graphviz.comment', $comment);
    }
    if ($label = $edge->getAttribute('migrate_visualize.label')) {
      $edge->setAttribute('graphviz.label', $label);
    }
    $edgeType = $edge->getVertexStart()->getAttribute('migrate_visualize.type');
    if (isset($edgeTypes[$edgeType])) {
      foreach ($edgeTypes[$edgeType] as $attributeKey => $attributeValue) {
        $edge->setAttribute("graphviz.{$attributeKey}", $attributeValue);
      }
    }
  }

  // @todo Find a way to set the group titles!
  // $graph->getGroup(0)->setAttribute('graphviz.title', 'Source')
  /** @var \Graphp\GraphViz\GraphViz $graphViz */
  try {
    $graphViz = new GraphViz();
    $graphViz->setFormat('svg');
    $variables['graphviz'] = $graphViz->createImageHTML($graph);
  }
  catch (UnexpectedValueException $exception) {
    \Drupal::messenger()->addError(t('Unable to invoke graphviz commands. <a href="@docs">Please check GraphViz is installed</a>.', [
      '@docs' => 'https://git.drupalcode.org/project/migrate_visualize/-/tree/1.0.x#graphviz-server-install',
    ]));
  }
  catch (Exception $exception) {
    \Drupal::messenger()->addError($exception->getMessage());
  }
}

/**
 * Prepares variables for migration-visualize template.
 *
 * Default template: migration-visualize-mermaid.html.twig.
 *
 * @param array $variables
 *   Variables array.
 */
function template_preprocess_migration_visualize_mermaid(array &$variables) {
  /** @var \Fhaculty\Graph\Graph $graph */
  $graph = $variables['graph'];

  /** @var \Fhaculty\Graph\Vertex $vertex */
  foreach ($graph->getVertices() as $k => $vertex) {
    if ($type = $vertex->getAttribute('migrate_visualize.type')) {
      $groups = ['source' => 0, 'process' => 1, 'destination' => 2];
      $groupId = explode(':', $type)[0];
      $vertex->setAttribute('migrate_visualize.group', $groupId);
      if (isset($groups[$groupId])) {
        $vertex->setGroup($groups[$groupId]);
      }
    }
  }
}

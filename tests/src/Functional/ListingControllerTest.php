<?php

namespace Drupal\Tests\migrate_visualize\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests Migrate Visualize settings form.
 *
 * @group migrate_visualize
 */
class ListingControllerTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'node',
    'migrate',
    'menu_ui',
    'migrate_plus',
    'migrate_visualize',
    'migrate_visualize_test',
    'path',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with access.
   *
   * @var \Drupal\user\Entity\User
   */
  private $trustedUser;

  /**
   * An untrusted user.
   *
   * @var \Drupal\user\Entity\User
   */
  private $untrustedUser;

  /**
   * Perform initial setup tasks that run before every test method.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setUp() : void {
    parent::setUp();
    $this->trustedUser = $this->drupalCreateUser(['access migrate_visualize']);
    $this->untrustedUser = $this->drupalCreateUser(['access content']);
  }

  /**
   * Test access to migrate visualize settings form.
   */
  public function testListingControllerAccess() {
    // Check access to migrate visualize form.
    $this->drupalLogin($this->untrustedUser);
    $this->drupalGet('/admin/reports/migrate-visualize');
    $this->assertSession()->statusCodeEquals('403');

    // Test as trusted user.
    $this->drupalLogin($this->trustedUser);
    $this->drupalGet('/admin/reports/migrate-visualize');
    $this->assertSession()->statusCodeEquals('200');
  }

  /**
   * Tests the listing controller.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testListingController() {
    // Login.
    $this->drupalLogin($this->trustedUser);

    // Access config page.
    $this->drupalGet('admin/reports/migrate-visualize');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->pageTextContains('Broken migration');
    $this->assertSession()->pageTextContains('Dummy migration');
    $this->assertSession()->pageTextContains('Fruit Terms');
  }

}

<?php

namespace Drupal\Tests\migrate_visualize\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests Migrate Visualize settings form.
 *
 * @group migrate_visualize
 */
class VisualizeControllerTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'node',
    'migrate',
    'migrate_plus',
    'migrate_visualize',
    'migrate_visualize_test',
    'path',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with access.
   *
   * @var \Drupal\user\Entity\User
   */
  private $trustedUser;

  /**
   * A user without access.
   *
   * @var \Drupal\user\Entity\User
   */
  private $untrustedUser;

  /**
   * Perform initial setup tasks that run before every test method.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setUp() : void {
    parent::setUp();
    $this->trustedUser = $this->drupalCreateUser(['access migrate_visualize']);
    $this->untrustedUser = $this->drupalCreateUser(['access content']);
  }

  /**
   * Test access denied to report listing for untrusted user.
   */
  public function testVisualizeControllerAccessDenied() {
    $this->drupalLogin($this->untrustedUser);
    $this->drupalGet('/admin/reports/migrate-visualize');
    $this->assertSession()->statusCodeEquals('403');
  }

  /**
   * Test access permitted to report listing for trusted user.
   */
  public function testVisualizeControllerAccessPermitted() {
    $this->drupalLogin($this->trustedUser);
    $this->drupalGet('/admin/reports/migrate-visualize');
    $this->assertSession()->statusCodeEquals('200');
  }

  /**
   * Tests the listing controller.
   *
   * @dataProvider visualizeControllerProvider
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testVisualizeControllerOutput($migration_id, $status_code, $expected_text) {
    $this->drupalLogin($this->trustedUser);
    $this->drupalGet("admin/reports/migrate-visualize/{$migration_id}");
    $this->assertSession()->statusCodeEquals($status_code);
    $this->assertSession()->pageTextContains($expected_text);
  }

  /**
   * Cases for testVisualizeControllerOutput.
   */
  public static function visualizeControllerProvider(): array {
    $cases = [];
    $cases[] = ['fruit_terms', 200, 'Fruit Terms'];
    $cases[] = ['does_not_exist', 404, 'Migration not found'];
    /* $cases[] = ['broken', 200, 'Unable to graph migration']; */
    return $cases;
  }

}

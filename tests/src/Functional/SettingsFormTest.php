<?php

namespace Drupal\Tests\migrate_visualize\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests Migrate Visualize settings form.
 *
 * @group migrate_visualize
 */
class SettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'node',
    'migrate',
    'migrate_visualize',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with access.
   *
   * @var \Drupal\user\Entity\User
   */
  private $trustedUser;

  /**
   * An untrusted user.
   *
   * @var \Drupal\user\Entity\User
   */
  private $untrustedUser;

  /**
   * Perform initial setup tasks that run before every test method.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setUp() : void {
    parent::setUp();
    $this->trustedUser = $this->drupalCreateUser(['access migrate_visualize']);
    $this->untrustedUser = $this->drupalCreateUser(['access content']);
  }

  /**
   * Test access to Migrate Visualize settings form.
   */
  public function testSettingsFormAccess() {
    // Check access to migrate visualize form.
    $this->drupalLogin($this->untrustedUser);
    $this->drupalGet('/admin/config/development/migrate-visualize');
    $this->assertSession()->statusCodeEquals('403');

    // Test as trusted user.
    $this->drupalLogin($this->trustedUser);
    $this->drupalGet('/admin/config/development/migrate-visualize');
    $this->assertSession()->statusCodeEquals('200');
  }

  /**
   * Tests the settings form.
   *
   * NB: This config form dynamically disables the GraphViz option if server
   * support is not detected. In some environments, the only valid form value
   * for display_mode may be "mermaidjs".
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testSettingsForm() {
    // Login.
    $this->drupalLogin($this->trustedUser);

    // Access config page.
    $this->drupalGet('admin/config/development/migrate-visualize');
    $this->assertSession()->statusCodeEquals(200);

    // Test the form elements exist and have defaults.
    $config = $this->config('migrate_visualize.settings');
    $this->assertSession()->fieldValueEquals(
      'display_mode',
      $config->get('display_mode'),
    );

    // Set non-default configuration via config API.
    $this->config('migrate_visualize.settings')
      ->set('display_mode', 'graphviz');

    // Test form submission.
    $this->submitForm(['display_mode' => 'mermaidjs'], 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    // Test the new values are there.
    $this->drupalGet('admin/config/development/migrate-visualize');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->fieldValueEquals(
      'display_mode',
      'mermaidjs',
    );
  }

}

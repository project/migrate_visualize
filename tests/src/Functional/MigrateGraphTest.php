<?php

namespace Drupal\Tests\migrate_visualize\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests Migrate Visualize settings form.
 *
 * @group migrate_visualize
 */
class MigrateGraphTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'node',
    'migrate',
    'migrate_visualize',
    'migrate_visualize_test',
    'path',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test access to migrate visualize settings form.
   */
  public function testMigrateGraphService() {
    $migrationPluginManager = \Drupal::service('plugin.manager.migration');
    $migrateGraphService = \Drupal::service('migrate_visualize.migrate_graph');

    $migration = $migrationPluginManager->createInstance('dummy');
    $migrateGraphService->graph($migration);
    $graph = $migrateGraphService->getGraph();

    $this->assertEquals(6, count($graph->getVertices()), 'dummy: number of vertices');
    $this->assertEquals(3, count($graph->getEdges()), 'dummy: number of edges');
  }

}

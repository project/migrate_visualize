# Migrate Visualize

Migrate Visualize aims to support migration development and review by providing an informative, high-level and stakeholder accessible interface to understanding migrations.

The goal is to visually represent the components of a migration.

## Usage

- Install and enable the module.
- Visit Admin > Configuration > Development > Migrate Visualize to configure the output format.
- Visit Admin > Reports > Migrate Visualize to view your migrations.

## Display formats

- [MermaidJS](https://mermaid-js.github.io/) runs in your browser, so can work out of the box (currently by using a CDN).
- [GraphViz](https://graphviz.org/) support requires installing the `graphviz` tool to your PHP. See [[Graphviz server install]]

## GraphViz server install

To use GraphViz charts instead of MermaidJS, make the `dot` and `graphviz` commands available to Drupal by installing [`graphviz`](https://graphviz.org/download/) package. Eg,

```
sudo apt install graphviz
```

If the module detects that this is not available, you'll see a warning "GraphViz can not be selected as the server requirements are not met." on the settings screen.

## Thanks

I'm really grateful for everyone who's put effort into making Drupal Migrate the tool it is today.

Special thanks to @danflanagan8 whose [Migrate Sandbox!](https://www.drupal.org/project/migrate_sandbox) project is a great tool for understanding Migrations, and was a point of reference when kicking this module off.

## Support

Ideas, bugs and improvements are welcome. Please use [the Drupal.org issue queue](https://www.drupal.org/project/issues/3257623).

## TODO

- [x] Generate simple Source, Destination and Process components
- [x] Generate a list of Source fields
  - [x] Source plugin has some base fields (but not all)
  - [x] Process plugin reveals other fields in use
- [x] Generate Process lines
- [x] Match source and destination via Process
- [x] Generate a list of Destination fields
  - [x] Process plugin shows destination fields
  - [ ] Inspect destination entity and retrieve fields / properties via Drupal API
- [ ] Indicate source metadata (eg plugin, config)
- [ ] Indicate destination metadata (plugin, entity type & config)
- [ ] Show additional detail about process lines?
- [ ] Indicate pseudofields
- [ ] Indicate unmatched source entries
- [ ] #3258777 Switch template used based on selected display mode
- [ ] #3258779 Add JS to copy generated charts as source for use in other systems (eg MermaidJS can be copied into Gitlab's Markdown)
- [ ] (wishlist) Drush cmd to output to terminal for Gitlab
- [ ] Improve layout / CSS for chart and source - crude at present
